public class Delaunay {
  int w;
  int h;
  Node n0;
  Node n1;
  Node n2;
  Node n3;
  List<Triangle> _triangles = new ArrayList<Triangle>();
  Delaunay(int w, int h) {
    this.w = w;
    this.h = h;
    n0 = new Node(0, 0);
    n1 = new Node(w, 0);
    n2 = new Node(w, h);
    n3 = new Node(0, h);
    this.clear();
  }

  List<Triangle> clear() {
    this._triangles.add(new Triangle(n0, n1, n2));
    this._triangles.add(new Triangle(n0, n2, n3));
    return this._triangles;
  }

  void insert(List<PVector> points) {
    List<Edge> edges = new ArrayList<Edge>();
    List<Triangle> tempTri = new ArrayList<Triangle>();
    ArrayList<Edge> polygon = new ArrayList<Edge>();
    for (PVector point : points) {
      tempTri.clear();
      edges.clear();
      float x = point.x;
      float y = point.y;
      int ilen = this._triangles.size();

      for (int i = 0; i < ilen; i++) {
        Triangle t = this._triangles.get(i);
        Circle circle = t.circle;
        float dx = circle.x - x;
        float dy = circle.y - y;
        float distSq = dx * dx + dy * dy;
        if (distSq < circle.rSq) {
          edges.add(t.e0);
          edges.add(t.e1);
          edges.add(t.e2);
        } else {
          tempTri.add(t);
        }
      }
      polygon.clear();
      //polygon = new ArrayList<Edge>(edges);
      //println(edges.size());
      
      for(int l = edges.size() - 1; l >= 0; l--) {
        boolean outerContinue = false;
        Edge edge = edges.get(l);
        for(int k = polygon.size() - 1; k >=0; k--) {
          Edge edgeo = polygon.get(k);
          if(edge.eq(edgeo)) {
            polygon.remove(k);
            outerContinue = true;
            break;
          }
        }
        if(!outerContinue) {
          polygon.add(edge);
        }
        
      }


      //println(polygon.size());
      for (Edge edge : polygon) {
        tempTri.add(new Triangle(edge.n0, edge.n1, new Node(x, y)));
      }

      this._triangles = new ArrayList<Triangle>(tempTri);
      ;
    }
  }

  //void draw() {
  //  for (Triangle t : this._triangles) {
  //    t.draw();
  //  }
  //}


  //            insert: function(points) {
  // i, ilen, j, jlen;
  //                var triangles, t, temps, edges, edge, polygon;
  //                var x, y, circle, dx, dy, distSq;            
  //                for (k = 0, klen = points.length; k < klen; k++) {
  //                    x = points[k][0];
  //                    y = points[k][1];                
  //                    triangles = this._triangles;
  //                    temps = [];
  //                    edges = [];

  //                    polygon = [];
  //                    // 辺の重複をチェック, 重複する場合は削除する
  //                    edgesLoop: for (ilen = edges.length, i = 0; i < ilen; i++) {
  //                        edge = edges[i];
  //                        // 辺を比較して重複していれば削除
  //                        for (jlen = polygon.length, j = 0; j < jlen; j++) {
  //                            if (edge.eq(polygon[j])) {
  //                                polygon.splice(j, 1);
  //                                continue edgesLoop;
  //                            }
  //                        }
  //                        polygon.push(edge);
  //                    }
  //                    for (ilen = polygon.length, i = 0; i < ilen; i++) {
  //                        edge = polygon[i];
  //                        temps.push(new Triangle(edge.nodes[0], edge.nodes[1], new Node(x, y)));
  //                    }
  //                    this._triangles = temps;
  //                }        
  //                return this;
  //            },   
  //            getTriangles: function() {
  //                return this._triangles.slice();
  //            }
}


//        /**
//         * Delaunay
//         * 
//         * @param {Number} width
//         * @param {Number} height
//         */
//        function Delaunay(width, height) {
//            this.width = width;
//            this.height = height;

//            this._triangles = null;

//            this.clear();
//        }

//        Delaunay.prototype = {

//            clear: function() {
//                var p0 = new Node(0, 0);
//                var p1 = new Node(this.width, 0);
//                var p2 = new Node(this.width, this.height);
//                var p3 = new Node(0, this.height);

//                this._triangles = [
//                    new Triangle(p0, p1, p2),
//                    new Triangle(p0, p2, p3)
//                ];

//                return this;
//            },

//            insert: function(points) {
//                var k, klen, i, ilen, j, jlen;
//                var triangles, t, temps, edges, edge, polygon;
//                var x, y, circle, dx, dy, distSq;

//                for (k = 0, klen = points.length; k < klen; k++) {
//                    x = points[k][0];
//                    y = points[k][1];

//                    triangles = this._triangles;
//                    temps = [];
//                    edges = [];

//                    for (ilen = triangles.length, i = 0; i < ilen; i++) {
//                        t = triangles[i];

//                        // 座標が三角形の外接円に含まれるか調べる
//                        circle  = t.circle;
//                        dx = circle.x - x;
//                        dy = circle.y - y;
//                        distSq = dx * dx + dy * dy;

//                        if (distSq < circle.radiusSq) {
//                            // 含まれる場合三角形の辺を保存
//                            edges.push(t.edges[0], t.edges[1], t.edges[2]);
//                        } else {
//                            // 含まれない場合は持ち越し
//                            temps.push(t);
//                        }
//                    }

//                    polygon = [];

//                    // 辺の重複をチェック, 重複する場合は削除する
//                    edgesLoop: for (ilen = edges.length, i = 0; i < ilen; i++) {
//                        edge = edges[i];

//                        // 辺を比較して重複していれば削除
//                        for (jlen = polygon.length, j = 0; j < jlen; j++) {
//                            if (edge.eq(polygon[j])) {
//                                polygon.splice(j, 1);
//                                continue edgesLoop;
//                            }
//                        }

//                        polygon.push(edge);
//                    }

//                    for (ilen = polygon.length, i = 0; i < ilen; i++) {
//                        edge = polygon[i];
//                        temps.push(new Triangle(edge.nodes[0], edge.nodes[1], new Node(x, y)));
//                    }

//                    this._triangles = temps;
//                }

//                return this;
//            },

//            getTriangles: function() {
//                return this._triangles.slice();
//            }
//        };

//        Delaunay.Node = Node;

//        return Delaunay;

//    })();
