import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.ListIterator;
PImage image;
PImage modified;
PImage screen;
int w = 720;
int h = 720;
float[] matrix;
List<PVector> tempPoints = new ArrayList<PVector>();
List<PVector> points = new ArrayList<PVector>();
float EDGE_DETECT_VALUE = 1;
float POINT_RATE = 0.075;
float POINT_MAX_NUM = 10000;
int BLUR_SIZE = 3;
int EDGE_SIZE = 3;
void settings() {
  size(w * 2, h);
}
void setup() {
  image = loadImage("portrait.jpg");
  image.loadPixels();
  modified = grayScale(image);
  modified = filter(modified, "blur");
  modified = filter(modified, "edge");
  tempPoints = getEdgePoints(modified);
  image(image, 0, 0);
  image(modified, w, 0);
  //for(PVector point : tempPoints) {
  //  stroke(255, 0, 0);
  //  point(point.x, point.y);
  //}
  int detectionNum = tempPoints.size();
  int ilen = tempPoints.size();
  int tlen = tempPoints.size();
  int i = 0;
  int j = 0;
  float limit = round(ilen * POINT_RATE);

  if (limit > POINT_MAX_NUM) {
    limit = POINT_MAX_NUM;
  }

  while (i < limit && i < ilen) {
    j = floor(tlen * random(1));
    points.add(tempPoints.get(j));
    tempPoints.remove(j);
    tlen--;
    i++;
  }


  Delaunay del = new Delaunay(w, h);
  del.insert(points);
  strokeWeight(1);
  //del.draw();
  for(Triangle t : del._triangles) {
    float cx = (t.n0.x + t.n1.x + t.n2.x) * 0.33333;
    float cy = (t.n0.y + t.n1.y + t.n2.y) * 0.33333;
    int index = (floor(cx) + floor(cy) * w);
    color colr = image.pixels[index];
    t.draw(colr);
  }
}

void draw() {
  noFill();
}

ArrayList<PVector> getEdgePoints(PImage image) {
  int width = image.width;
  int height = image.height;
  image.loadPixels();
  ArrayList<PVector> tempPoints = new ArrayList<PVector>();
  for (int y = 0; y < height; y++) {
    for (int x = 0; x < width; x++) {
      float total = 0, sum = 0;
      for (int row = -1; row <= 1; row++) {
        int sy = y + row;
        int step = sy * width;
        if (sy >= 0 && sy < height) {
          for (int col = -1; col <= 1; col++) {
            int sx = x + col;
            if (sx >= 0 && sx < width) {
              sum += brightness(image.pixels[sx + step]);
              total++;
            }
          }
        }
      }
      if (total > 0) {
        sum /= total;
      }
      if (sum > EDGE_DETECT_VALUE) {
        tempPoints.add(new PVector(x, y));
      }
    }
  }
  return tempPoints;
}

float[] blurMatrix() {
  int side = BLUR_SIZE * 2 + 1;
  int sideSq = side * side;
  float[] matrix = new float[sideSq];
  for (int i = 0; i < sideSq; i++) {
    matrix[i] = 1;
  }
  return matrix;
}

float[] edgeMatrix() {
  int side = EDGE_SIZE * 2 + 1;
  int sideSq = side * side;
  float[] matrix = new float[sideSq];
  float center = floor(sideSq * 0.5);
  for (int i = 0; i < sideSq; i++) {
    matrix[i] = i == center ? -sideSq + 1 : 1;
  }
  return matrix;
}

PImage grayScale(PImage image) {
  int width  = image.width;
  int height = image.height;
  PImage modified = new PImage(width, height);
  image.loadPixels();
  modified.loadPixels();
  for (int y = 0; y < height; y++) {
    int step = y * width;
    for (int x = 0; x < width; x++) {
      int i = x + step;
      modified.pixels[i] = color(brightness(image.pixels[i]));
    }
  }
  modified.updatePixels();
  return modified;
}


PImage filter(PImage image, String type) {
  int width  = image.width;
  int height = image.height;
  color thisCol = color(255, 255, 255);
  PImage modified = new PImage(width, height);
  image.loadPixels();
  modified.loadPixels();
  float[] matrix = type == "blur" ? blurMatrix() : edgeMatrix();
  float divisor = type == "blur" ? matrix.length : 1;
  float divisorScalar = 1 / divisor;
  for (int i = 0; i < divisor; i++) {
    matrix[i] *= divisorScalar;
  }
  //println(Arrays.toString(matrix));

  int size = floor(sqrt(matrix.length));
  int range = floor(size * 0.5);

  for (int fully = 0; fully < height; fully++) {
    int fullStep = fully * width;
    for (int fullx = 0; fullx < width; fullx++) {
      int fullIndex = fullx + fullStep;
      float r = 0;
      float g = 0;
      float b = 0;
      float newr = 0;
      float newg = 0;
      float newb = 0;
      for (int y = -range; y <= range; y++) {
        int matrixy = fully + y;
        int jstep = matrixy * width;
        int kstep = (y + range) * size;
        if (matrixy >= 0 && matrixy < height) {
          for (int x = -range; x <= range; x++) {
            int matrixx = fullx + x;
            if (matrixx >= 0 && matrixx < width) {
              float v = matrix[(x + range) + kstep];
              thisCol = image.pixels[matrixx + jstep];
              r = red(thisCol);
              newr += r * v;
              g = green(thisCol);
              newg += g * v;
              b = blue(thisCol);
              newb += b * v;
            }
          }
        }
      }
      if (newr < 0) {
        newr = 0;
      } else if (newr > 255) {
        newr = 255;
      }
      if (newg < 0) {
        newg = 0;
      } else if (newg > 255) {
        newg = 255;
      }
      if (newb < 0) {
        newb = 0;
      } else if (newb > 255) {
        newb = 255;
      }
      color newCol = color(newr, newg, newb);
      modified.pixels[fullIndex] =  newCol;
      //r & 0xFF;
    }
  }
  modified.updatePixels();
  return modified;
}

void mouseClicked() {
  image.loadPixels();
  for (int i = -20; i <= 20; i++) {
    int x = mouseX + i;
    if (x >= 0 && x < w) {
      for (int j = -20; j <= 20; j++) {
        int y = mouseY + j;
        if (y >= 0 && x < h) {
          color originalColor = image.pixels[x + y * image.width];
          stroke(originalColor);
          //strokeWeight(25);
          println(x);

          modified.pixels[x + y * w] = originalColor;
        }
      }
    }
  }
  modified.updatePixels();
}
