public class Edge {
  Node n0;
  Node n1;
  Edge(Node n0, Node n1) {
    this.n0 = n0;
    this.n1 = n1;
  }
  
  boolean eq(Edge edge) {
    return (this.n0.eq(edge.n0) && this.n1.eq(edge.n1)) || (this.n0.eq(edge.n1) && this.n1.eq(edge.n0));
  }
  
  void draw() {
    stroke(255, 50);
    line(this.n0.x, this.n0.y, this.n1.x, this.n1.y);
  }
}
