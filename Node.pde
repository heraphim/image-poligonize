public class Node {
  float x;
  float y;
  int id;
  Node(float x, float y) {
    this.x = x;
    this.y = y;
    this.id = -1;
  }
  Node(float x, float y, int id) {
    this.x = x;
    this.y = y;
    this.id = id;
  }
  boolean eq(Node n) {
    float dx = this.x - n.x;
    float dy = this.y - n.y;
    return (dx < 0 ? -dx : dx) < 0.0001 && (dy < 0 ? -dy : dy) < 0.0001;
  }
  String toString() {
    return "(x: " + this.x + ", y: " + this.y + ")";
  }
}
