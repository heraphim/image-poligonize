public class Triangle {
  Node n0;
  Node n1;
  Node n2;
  Edge e0;
  Edge e1;
  Edge e2;
  Circle circle;
  Triangle(Node n0, Node n1, Node n2) {
    this.n0 = n0;
    this.n1 = n1;
    this.n2 = n2;
    this.e0 = new Edge(n0, n1);
    this.e1 = new Edge(n1, n2);
    this.e2 = new Edge(n2, n0);

    float ax = n1.x - n0.x;
    float ay = n1.y - n0.y;
    float bx = n2.x - n0.x;
    float by = n2.y - n0.y;
    float t = n1.x * n1.x - n0.x * n0.x + n1.y * n1.y - n0.y * n0.y;
    float u = n2.x * n2.x - n0.x * n0.x + n2.y * n2.y - n0.y * n0.y;
    float s = 1 / (2 * (ax * by - ay * bx));
    float x = ((n2.y - n0.y) * t + (n0.y - n1.y) * u) * s;
    float y = ((n0.x - n2.x) * t + (n1.x - n0.x) * u) * s;
    float dx = n0.x - x;
    float dy = n0.y - y;
    float rSq = dx * dx + dy * dy;
    this.circle = new Circle(x, y, rSq);
  }
  
  void draw(color colr) {
    fill(colr);
    noStroke();
    beginShape();
    vertex(this.n0.x, this.n0.y);
    vertex(this.n1.x, this.n1.y);
    vertex(this.n2.x, this.n2.y);
    endShape();
  }
}
